/*
 * Copyright (C) 2013,2014,2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.util.Iterator;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class StringListTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class StringListTest
{
	public final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(StringListTest.class);

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);
	}

	/**
	 * Test char at 01.
	 */
	@Test
	public void testCharAt01()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("abcdefghijklm");

		//
		char target = source.charAt(0);
		Assert.assertEquals(target, 'a');

		//
		target = source.charAt(4);
		Assert.assertEquals(target, 'e');

		//
		target = source.charAt(11);
		Assert.assertEquals(target, 'l');

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test char at 02.
	 */
	@Test
	public void testCharAt02()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("abc");
		source.append("def");
		source.append("ghi");
		source.append("jkl");
		source.append("mno");

		//
		char target = source.charAt(0);
		Assert.assertEquals('a', target);

		//
		target = source.charAt(4);
		Assert.assertEquals('e', target);

		//
		target = source.charAt(11);
		Assert.assertEquals('l', target);

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test char at exception 01.
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testCharAtException01()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("abcdefghijklm");

		//
		char target = source.charAt(-2);
		Assert.assertEquals('a', target);

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test char at exception 02.
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testCharAtException02()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("abcdefghijklm");

		//
		char target = source.charAt(100);
		Assert.assertEquals('a', target);

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test char at exception 03.
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testCharAtException03()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("abcdefghijklm");

		//
		char target = source.charAt(source.length());
		Assert.assertEquals('a', target);

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test constructor 01.
	 */
	@Test
	public void testConstructor01()
	{
		//
		this.logger.debug("===== test starting...");

		String[] source = { "a", "b", "c" };

		//
		StringList target = new StringList(source);

		Assert.assertEquals(3, target.size());

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test constructor 02.
	 */
	@Test
	public void testConstructor02()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList target = new StringList("a", "b", "c");

		Assert.assertEquals(3, target.size());

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains 01.
	 */
	@Test
	public void testContains01()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.contains("abc"));
		Assert.assertTrue(buffer.contains("def"));
		Assert.assertTrue(buffer.contains("ghi"));

		Assert.assertFalse(buffer.contains("lmn"));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains 02.
	 */
	@Test
	public void testContains02()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.contains(new StringBuffer().append("abc").toString()));
		Assert.assertTrue(buffer.contains(new StringBuffer().append("def")));
		Assert.assertTrue(buffer.contains(new StringBuffer().append("ghi")));

		Assert.assertFalse(buffer.contains(new StringBuffer().append("lmn")));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains 02.
	 */
	@Test
	public void testContains03()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.contains(new StringList().append("abc")));
		Assert.assertTrue(buffer.contains(new StringList().append("def")));
		Assert.assertTrue(buffer.contains(new StringList().append("ghi")));

		Assert.assertFalse(buffer.contains(new StringList().append("lmn")));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains any 01.
	 */
	@Test
	public void testContainsAny01()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.containsAny("abc"));
		Assert.assertTrue(buffer.containsAny("def"));
		Assert.assertTrue(buffer.containsAny("ghi"));

		Assert.assertFalse(buffer.containsAny("aaa"));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains any 02.
	 */
	@Test
	public void testContainsAny02()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.containsAny("aaa", "bbb", "abc"));
		Assert.assertTrue(buffer.containsAny("aaa", "bbb", "def"));
		Assert.assertTrue(buffer.containsAny("aaa", "bbb", "ghi"));

		Assert.assertFalse(buffer.containsAny("aaa", "bbb", "ccc"));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains any 03.
	 */
	@Test
	public void testContainsAny03()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.containsAny(new StringList("aaa", "bbb", "abc")));
		Assert.assertTrue(buffer.containsAny(new StringList("aaa", "bbb", "def")));
		Assert.assertTrue(buffer.containsAny(new StringList("aaa", "bbb", "ghi")));

		Assert.assertFalse(buffer.containsAny(new StringList("aaa", "bbb", "aBc")));
		Assert.assertFalse(buffer.containsAny(new StringList("aaa", "bbb", "ccc")));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains any 01.
	 */
	@Test
	public void testContainsAnyIgnoreCase01()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.containsAnyIgnoreCase("aaa", "bbb", "abc"));
		Assert.assertFalse(buffer.containsAnyIgnoreCase("aaa", "bbb", "dbf"));
		Assert.assertTrue(buffer.containsAnyIgnoreCase("aaa", "bbb", "ghi"));
		Assert.assertTrue(buffer.containsAnyIgnoreCase("aaa", "bbb", "Abc"));
		Assert.assertTrue(buffer.containsAnyIgnoreCase("aaa", "bbb", "dEf"));
		Assert.assertTrue(buffer.containsAnyIgnoreCase("aaa", "bbb", "ghI"));

		Assert.assertFalse(buffer.containsAnyIgnoreCase("aaa", "bbb", "ccc"));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test contains any ignore case 02.
	 */
	@Test
	public void testContainsAnyIgnoreCase02()
	{
		//
		this.logger.debug("===== test starting...");

		StringList buffer = new StringList();
		buffer.append("abc");
		buffer.append("def");
		buffer.append("ghi");

		Assert.assertTrue(buffer.containsAnyIgnoreCase(new StringList("aaa", "bbb", "abc")));
		Assert.assertTrue(buffer.containsAnyIgnoreCase(new StringList("aaa", "bbb", "def")));
		Assert.assertTrue(buffer.containsAnyIgnoreCase(new StringList("aaa", "bbb", "ghi")));
		Assert.assertTrue(buffer.containsAnyIgnoreCase(new StringList("aaa", "bbb", "Abc")));
		Assert.assertTrue(buffer.containsAnyIgnoreCase(new StringList("aaa", "bbb", "dEf")));
		Assert.assertTrue(buffer.containsAnyIgnoreCase(new StringList("aaa", "bbb", "ghI")));

		Assert.assertFalse(buffer.containsAnyIgnoreCase(new StringList("aaa", "bbb", "ccc")));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test iterator of char 01.
	 */
	@Test
	public void testIteratorOfChar01()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("abc");
		source.append("def");
		source.append("ghi");
		source.append("jkl");
		source.append("mno");

		//
		Iterator<Character> iterator = source.iteratorOfChar();

		for (int index = 0; index < source.length(); index++)
		{
			StringListCharPosition position = ((StringListCharIterator) iterator).nextPosition();
			System.out.println(index + ":" + source.charAt(index) + " " + position.getCharIndex() + " " + position.getStringIndex() + " " + position.getLocalCharIndex());

			Assert.assertTrue(iterator.hasNext());

			Character character = ((StringListCharIterator) iterator).next();

			Assert.assertEquals(character, new Character(source.charAt(index)));
		}

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test shrink 01.
	 */
	@Test
	public void testShrink01()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();

		Assert.assertEquals(0, source.size());

		source.shrink();

		Assert.assertEquals(0, source.size());

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test shrink 01.
	 */
	@Test
	public void testShrink02()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();

		source.append("alpha");
		source.append("bravo");
		source.append("charlie");
		source.append("delta");

		Assert.assertEquals(4, source.size());

		source.shrink();

		Assert.assertEquals(1, source.size());
		Assert.assertEquals("alphabravocharliedelta", source.get(0));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test substring 01.
	 */
	@Test
	public void testSubstring01()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("hamburger");

		//
		StringList target = source.substring(4, 8);
		Assert.assertEquals("urge", target.toString());

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test substring 02.
	 */
	@Test
	public void testSubstring02()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("ham").append("bur").append("ger");

		//
		StringList target = source.substring(4, 8);
		Assert.assertEquals("urge", target.toString());

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test substring 03.
	 */
	@Test
	public void testSubstring03()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("smiles");

		//
		StringList target = source.substring(1, 5);
		Assert.assertEquals("mile", target.toString());

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test substring 04.
	 */
	@Test
	public void testSubstring04()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList source = new StringList();
		source.append("sm").append("il").append("es");

		//
		StringList target = source.substring(1, 5);
		Assert.assertEquals("mile", target.toString());

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test to string 01.
	 */
	@Test
	public void testToString01()
	{
		//
		this.logger.debug("===== test starting...");

		//
		StringList buffer = new StringList();

		String target = buffer.toString();

		Assert.assertTrue(target.equals(""));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test to string 02.
	 */
	@Test
	public void testToString02()
	{
		//
		this.logger.debug("===== test starting...");

		//
		String source = "abcdefghijklm";

		StringList buffer = new StringList();
		buffer.append(source);

		String target = buffer.toString();

		Assert.assertEquals(source, target);
		Assert.assertTrue(target.equals(source));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test to string 03.
	 */
	@Test
	public void testToString03()
	{
		//
		this.logger.debug("===== test starting...");

		//
		String source1 = "abcdefghijklm";
		String source2 = "other stuff";

		StringList buffer = new StringList();
		buffer.append(source1);
		buffer.append(source2);

		String target = buffer.toString();

		Assert.assertTrue(target.equals(source1 + source2));

		//
		this.logger.debug("===== test done.");
	}
}
