/*
 * Copyright (C) 2013,2014,2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class StringsUtilsTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class StringsUtilsTest
{
	public final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(StringsUtilsTest.class);

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);
	}

	/**
	 * Test contains any 01.
	 */
	@Test
	public void testContainsAny01()
	{
		Assert.assertTrue(StringsUtils.containsAny("abc", "aaa", "bbb", "abc"));
		Assert.assertFalse(StringsUtils.containsAny("abc", "aaa", "bbb", "aBc"));
	}

	/**
	 * Test contains any ignore case 01.
	 */
	@Test
	public void testContainsAnyIgnoreCase01()
	{
		Assert.assertTrue(StringsUtils.containsAnyIgnoreCase("abc", "aaa", "bbb", "abc"));
		Assert.assertTrue(StringsUtils.containsAnyIgnoreCase("abc", "aaa", "bbb", "aBc"));
		Assert.assertFalse(StringsUtils.containsAnyIgnoreCase("abc", "aaa", "bbb", "ccc"));
	}

	/**
	 * Test contains blank 01.
	 */
	@Test
	public void testContainsBlank01()
	{
		Assert.assertFalse(StringsUtils.containsBlank((Collection<String>) null));
		Assert.assertFalse(StringsUtils.containsBlank(new StringList("aaa", "bbb", "ccc")));
		Assert.assertTrue(StringsUtils.containsBlank(new StringList("aaa", null, "ccc")));
		Assert.assertTrue(StringsUtils.containsBlank(new StringList("aaa", "", "ccc")));
		Assert.assertTrue(StringsUtils.containsBlank(new StringList("aaa", "  ", "ccc")));
	}

	/**
	 * Test contains blank 02.
	 */
	@Test
	public void testContainsBlank02()
	{
		Assert.assertFalse(StringsUtils.containsBlank((String[]) null));
		Assert.assertFalse(StringsUtils.containsBlank("aaa", "bbb", "ccc"));
		Assert.assertTrue(StringsUtils.containsBlank("aaa", null, "ccc"));
		Assert.assertTrue(StringsUtils.containsBlank("aaa", "", "ccc"));
		Assert.assertTrue(StringsUtils.containsBlank("aaa", "   ", "ccc"));
	}

	/**
	 * Test contains blank 03.
	 */
	@Test
	public void testContainsBlank03()
	{
		Assert.assertFalse(StringsUtils.containsBlank((ArrayList<String>) null));
		ArrayList<String> source = new ArrayList<String>();
		source.add("aaa");
		source.add("bbb");
		source.add("ccc");
		Assert.assertFalse(StringsUtils.containsBlank(source));
		source.set(1, null);
		Assert.assertTrue(StringsUtils.containsBlank(source));
		source.set(1, "");
		Assert.assertTrue(StringsUtils.containsBlank(source));
		source.set(1, "   ");
		Assert.assertTrue(StringsUtils.containsBlank(source));
	}

	/**
	 * Test contains empty 01.
	 */
	@Test
	public void testContainsEmpty01()
	{
		Assert.assertFalse(StringsUtils.containsEmpty((Collection<String>) null));
		Assert.assertFalse(StringsUtils.containsEmpty(new StringList("aaa", "bbb", "ccc")));
		Assert.assertTrue(StringsUtils.containsEmpty(new StringList("aaa", null, "ccc")));
		Assert.assertTrue(StringsUtils.containsEmpty(new StringList("aaa", "", "ccc")));
	}

	/**
	 * Test contains empty 02.
	 */
	@Test
	public void testContainsEmpty02()
	{
		Assert.assertFalse(StringsUtils.containsEmpty((String[]) null));
		Assert.assertFalse(StringsUtils.containsEmpty("aaa", "bbb", "ccc"));
		Assert.assertTrue(StringsUtils.containsEmpty("aaa", null, "ccc"));
		Assert.assertTrue(StringsUtils.containsEmpty("aaa", "", "ccc"));
	}

	/**
	 * Test contains empty 03.
	 */
	@Test
	public void testContainsEmpty03()
	{
		Assert.assertFalse(StringsUtils.containsEmpty((ArrayList<String>) null));
		ArrayList<String> source = new ArrayList<String>();
		source.add("aaa");
		source.add("bbb");
		source.add("ccc");
		Assert.assertFalse(StringsUtils.containsEmpty(source));
		source.set(1, null);
		Assert.assertTrue(StringsUtils.containsEmpty(source));
		source.set(1, "");
		Assert.assertTrue(StringsUtils.containsEmpty(source));
	}

	/**
	 * Test contains null 01.
	 */
	@Test
	public void testContainsNull01()
	{
		Assert.assertFalse(StringsUtils.containsNull((Collection<String>) null));
		Assert.assertFalse(StringsUtils.containsNull(new StringList("aaa", "bbb", "ccc")));
		Assert.assertTrue(StringsUtils.containsNull(new StringList("aaa", null, "ccc")));
	}

	/**
	 * Test contains null 02.
	 */
	@Test
	public void testContainsNull02()
	{
		Assert.assertFalse(StringsUtils.containsNull((String[]) null));
		Assert.assertFalse(StringsUtils.containsNull("aaa", "bbb", "ccc"));
		Assert.assertTrue(StringsUtils.containsNull("aaa", null, "ccc"));
	}

	/**
	 * Test contains null 03.
	 */
	@Test
	public void testContainsNull03()
	{
		Assert.assertFalse(StringsUtils.containsNull((ArrayList<String>) null));
		ArrayList<String> source = new ArrayList<String>();
		source.add("aaa");
		source.add("bbb");
		source.add("ccc");
		Assert.assertFalse(StringsUtils.containsNull(source));
		source.set(1, null);
		Assert.assertTrue(StringsUtils.containsNull(source));
	}

	@Test
	public void testEqualsAny01()
	{
		Assert.assertTrue(StringsUtils.equalsAny("abc", "aaa", "bbb", "abc"));
		Assert.assertFalse(StringsUtils.equalsAny("abc", "aaa", "bbb", "aBc"));
	}

	@Test
	public void testEqualsAnyIgnoreCase01()
	{
		Assert.assertTrue(StringsUtils.equalsAnyIgnoreCase("abc", "aaa", "bbb", "abc"));
		Assert.assertTrue(StringsUtils.equalsAnyIgnoreCase("abc", "aaa", "bbb", "aBc"));
	}

	/**
	 * Test find smallest 01.
	 */
	@Test
	public void testFindSmallest01()
	{
		// TODO
		Assert.assertFalse(StringsUtils.containsNull((ArrayList<String>) null));
		ArrayList<String> source = new ArrayList<String>();
		source.add("aaa");
		source.add("bbb");
		source.add("ccc");
		Assert.assertFalse(StringsUtils.containsNull(source));
		source.set(1, null);
		Assert.assertTrue(StringsUtils.containsNull(source));
	}

	/**
	 * Load to string list URL 01.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testLoadResource01() throws IOException
	{
		//
		StringList source = StringsUtils.load(StringsUtilsTest.class.getResource("/fr/devinsy/strings/lines.txt"));

		//
		Assert.assertEquals(4, source.size());
		Assert.assertEquals("trois", source.get(3 - 1));
	}
}
