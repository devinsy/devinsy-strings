/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.io.IOException;
import java.io.Writer;

/**
 * The Class StringListWriter.
 */
public class StringListWriter extends Writer
{
	private StringList out;

	/**
	 * Instantiates a new string list writer.
	 */
	public StringListWriter()
	{
		this.out = new StringList();
	}

	/**
	 * Instantiates a new string list writer.
	 * 
	 * @param initialCapacity
	 *            the initial capacity
	 */
	public StringListWriter(final int initialCapacity)
	{
		this.out = new StringList(initialCapacity);
	}

	/**
	 * Instantiates a new string list writer.
	 * 
	 * @param target
	 *            the target
	 */
	public StringListWriter(final StringList target)
	{
		if (target == null)
		{
			throw new NullPointerException("target is null.");
		}
		else
		{
			this.out = target;
		}
	}

	/* (non-Javadoc)
	 * @see java.io.Writer#close()
	 */
	/*
	 *
	 */
	@Override
	public void close() throws IOException
	{

	}

	/* (non-Javadoc)
	 * @see java.io.Writer#flush()
	 */
	/*
	 *
	 */
	@Override
	public void flush() throws IOException
	{

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = this.out.toString();

		//
		return result;
	}

	/**
	 * To string list.
	 * 
	 * @return the string list
	 */
	public StringList toStringList()
	{
		StringList result;

		result = this.out;

		//
		return result;
	}

	/**
	 * Write.
	 * 
	 * @param c
	 *            the c
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void write(final char c) throws IOException
	{
		this.out.append(c);
	}

	/* (non-Javadoc)
	 * @see java.io.Writer#write(char[], int, int)
	 */
	@Override
	public void write(final char[] cbuf, final int off, final int len) throws IOException
	{
		char[] target;
		if ((off == 0) && (cbuf.length == len))
		{
			target = cbuf;
		}
		else
		{
			target = new char[len];
			for (int index = off; index < len; index++)
			{
				target[index] = cbuf[index];
			}
		}

		//
		this.out.append(new String(target));
	}

	/* (non-Javadoc)
	 * @see java.io.Writer#write(java.lang.String)
	 */
	@Override
	public void write(final String string) throws IOException
	{
		this.out.append(string);
	}
}
