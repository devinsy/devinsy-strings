/*
 * Copyright (C) 2013-2015,2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.io.IOException;
import java.io.InputStream;

/**
 * The Class StringListInputStream.
 */
public class StringListInputStream extends InputStream
{
	private StringList in;

	/**
	 * Instantiates a new string list input stream.
	 */
	public StringListInputStream()
	{
		this.in = new StringList();
	}

	/**
	 * Instantiates a new string list input stream.
	 * 
	 * @param size
	 *            the size
	 */
	public StringListInputStream(final int size)
	{
		this.in = new StringList(size);
	}

	/**
	 * Instantiates a new string list input stream.
	 * 
	 * @param source
	 *            the source
	 */
	public StringListInputStream(final StringList source)
	{
		if (source == null)
		{
			throw new NullPointerException("source is null.");
		}
		else
		{
			this.in = source;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException
	{
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException
	{
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void mark(final int readlimit)
	{
		// TODO
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean markSupported()
	{
		boolean result;

		result = true;

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void reset() throws IOException
	{
		// TODO
		throw new IOException("mark/reset not supported");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long skip(final long n) throws IOException
	{
		// TODO
		return 0;
	}
}
