/*
 * Copyright (C) 2008-2010,2013-2015,2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

/**
 * The <code>StringList</code> class is a collection of String objects with
 * specific methods. It makes possible to build a string without any copy. The
 * goal is to optimize the building of strings where they are lot of
 * concatenation action.
 * 
 */
public class StringList extends ArrayList<String> implements CharSequence, Appendable
{
	private static final long serialVersionUID = -1154185934830213732L;

	public static final String LINE_SEPARATOR = "\n";

	/**
	 * Constructs an empty list with an initial capacity of ten (see ArrayList
	 * constructor).
	 */
	public StringList()
	{
		super();
	}

	/**
	 * Instantiates a new string list from an array of boolean.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final boolean... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (boolean value : source)
			{
				add(String.valueOf(value));
			}
		}
	}

	/**
	 * Instantiates a new string list from an array of char.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final char... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (char value : source)
			{
				add(String.valueOf(value));
			}
		}
	}

	/**
	 * Constructs a list of string of the specified collection, in the order they
	 * are returned by the collection's iterator.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final Collection<String> source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.size());

			for (String string : source)
			{
				this.add(string);
			}
		}
	}

	/**
	 * Instantiates a new string list from an array of double.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final double... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (double value : source)
			{
				add(String.valueOf(value));
			}
		}
	}

	/**
	 * Instantiates a new string list from an array of float.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final float... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (float value : source)
			{
				add(String.valueOf(value));
			}
		}
	}

	/**
	 * Constructs an empty list with the specified initial capacity.
	 *
	 * @param initialCapacity
	 *            the initial capacity
	 */
	public StringList(final int initialCapacity)
	{
		super(initialCapacity);
	}

	/**
	 * Instantiates a new string list from an array of int.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final int... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (int value : source)
			{
				add(String.valueOf(value));
			}
		}
	}

	/**
	 * Instantiates a new string list from an array of long.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final long... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (long value : source)
			{
				add(String.valueOf(value));
			}
		}
	}

	/**
	 * Constructs a list of string from an Object array.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final Object... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (Object value : source)
			{
				if (value == null)
				{
					add(null);
				}
				else
				{
					add(value.toString());
				}
			}
		}
	}

	/**
	 * Instantiates a new string list from an array of short.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final short... source)
	{
		super();

		if (source != null)
		{
			ensureCapacity(source.length);

			for (short value : source)
			{
				add(String.valueOf(value));
			}
		}
	}

	/**
	 * Constructs a list of string from a string array.
	 *
	 * @param source
	 *            the source
	 */
	public StringList(final String... source)
	{
		super();

		if (source != null)
		{
			//
			ensureCapacity(source.length);

			//
			for (String string : source)
			{
				this.add(string);
			}
		}
	}

	/**
	 * Appends the string of the array argument to this string list.
	 *
	 * @param values
	 *            the values
	 * @return the string list
	 */
	public StringList append(final boolean... values)
	{
		StringList result;

		if (values != null)
		{
			for (boolean value : values)
			{
				this.append(value);
			}
		}

		result = this;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Appendable#append(char)
	 */
	@Override
	public StringList append(final char character)
	{
		StringList result;

		this.add(String.valueOf(character));

		result = this;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Appendable#append(java.lang.CharSequence)
	 */
	@Override
	public StringList append(final CharSequence charSequence) throws IOException
	{
		StringList result;

		if (charSequence != null)
		{
			this.append(charSequence.toString());
		}

		result = this;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Appendable#append(java.lang.CharSequence, int, int)
	 */
	@Override
	public StringList append(final CharSequence charSequence, final int start, final int end) throws IOException
	{
		StringList result;

		if (charSequence != null)
		{
			this.append(charSequence.subSequence(start, end).toString());
		}

		result = this;

		//
		return result;
	}

	/**
	 * Appends the string of the specified collection, in the order they are
	 * returned by the collection's iterator.
	 *
	 * @param strings
	 *            the strings
	 * @return the string list
	 */
	public StringList append(final Collection<String> strings)
	{
		StringList result;

		if (strings != null)
		{
			for (String string : strings)
			{
				this.append(string);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * Appends the string representation of the double argument to this string list.
	 *
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList append(final double value)
	{
		StringList result;

		result = this.append(String.valueOf(value));

		//
		return result;
	}

	/**
	 * Appends the string representation of the int argument to this string list.
	 * 
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList append(final int value)
	{
		StringList result;

		result = this.append(String.valueOf(value));

		//
		return result;
	}

	/**
	 * Appends the string representation of the long argument to this string list.
	 *
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList append(final long value)
	{
		StringList result;

		result = this.append(String.valueOf(value));

		//
		return result;
	}

	/**
	 * Appends the string representation of the int argument to this string list.
	 *
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList append(final Object value)
	{
		StringList result;

		if (value != null)
		{
			this.append(value.toString());
		}

		result = this;

		//
		return result;
	}

	/**
	 * Appends the string of the array argument to this string list.
	 *
	 * @param values
	 *            the values
	 * @return the string list
	 */
	public StringList append(final Object... values)
	{
		StringList result;

		if (values != null)
		{
			for (Object value : values)
			{
				this.append(value);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * Appends the string representation of the long argument to this string list.
	 *
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList append(final short value)
	{
		StringList result;

		result = this.append(String.valueOf(value));

		//
		return result;
	}

	/**
	 * Appends the string argument to this string list.
	 *
	 * @param string
	 *            the string
	 * @return the string list
	 */
	public StringList append(final String string)
	{
		StringList result;

		if (string != null)
		{
			this.add(string);
		}

		result = this;

		//
		return result;
	}

	/**
	 * Appends the string of the array argument to this string list.
	 *
	 * @param strings
	 *            the strings
	 * @return the string list
	 */
	public StringList append(final String... strings)
	{
		StringList result;

		if (strings != null)
		{
			for (String string : strings)
			{
				this.append(string);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * Appends a break line to this string list.
	 *
	 * @return the string list
	 */
	public StringList appendln()
	{
		StringList result;

		this.add(LINE_SEPARATOR);

		result = this;

		//
		return result;
	}

	/**
	 * Appends the string representation of the char argument to this string list,
	 * then append a break line too.
	 *
	 * @param character
	 *            the character
	 * @return the string list
	 */
	public StringList appendln(final char character)
	{
		StringList result;

		result = this.append(character).appendln();

		//
		return result;
	}

	/**
	 * Appends the string of the specified collection, in the order they are
	 * returned by the collection's iterator. Then append a return line.
	 *
	 * @param string
	 *            the string
	 * @return the string list
	 */
	public StringList appendln(final Collection<String> string)
	{
		StringList result;

		result = this.append(string).appendln();

		//
		return result;
	}

	/**
	 * Appends the string representation of the double argument to this string list.
	 * 
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList appendln(final double value)
	{
		StringList result;

		result = this.append(value).appendln();

		//
		return result;
	}

	/**
	 * Appends the string representation of the int argument to this string list.
	 * 
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList appendln(final int value)
	{
		StringList result;

		result = this.append(value).appendln();

		//
		return result;
	}

	/**
	 * Appends the string representation of the long argument to this string list.
	 * 
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList appendln(final long value)
	{
		StringList result;

		result = this.append(value).appendln();

		//
		return result;
	}

	/**
	 * Appends the string of the array argument to this string list.
	 *
	 * @param values
	 *            the values
	 * @return the string list
	 */
	public StringList appendln(final Object... values)
	{
		StringList result;

		append(values).appendln();

		result = this;

		//
		return result;
	}

	/**
	 * Appends the string representation of the int argument to this string list.
	 * 
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList appendln(final Object value)
	{
		StringList result;

		result = this.append(value).appendln();

		//
		return result;
	}

	/**
	 * Appends the string representation of the long argument to this string list.
	 * 
	 * @param value
	 *            the value
	 * @return the string list
	 */
	public StringList appendln(final short value)
	{
		StringList result;

		result = this.append(value).appendln();

		//
		return result;
	}

	/**
	 * Appends the string argument to this string list.
	 * 
	 * @param string
	 *            the string
	 * @return the string list
	 */
	public StringList appendln(final String string)
	{
		StringList result;

		result = this.append(string).appendln();

		//
		return result;
	}

	/**
	 * Appends the string of the array argument to this string list.
	 * 
	 * @param strings
	 *            the strings
	 * @return the string list
	 */
	public StringList appendln(final String... strings)
	{
		StringList result;

		result = this.append(strings).appendln();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public char charAt(final int index)
	{
		char result;

		//
		StringListCharPosition position = indexOf(index);

		//
		result = get(position.getStringIndex()).charAt(position.getLocalCharIndex());

		//
		return result;
	}

	/**
	 * Char at.
	 * 
	 * @param position
	 *            the position
	 * @return the char
	 */
	public char charAt(final StringListCharPosition position)
	{
		char result;

		//
		result = get(position.getStringIndex()).charAt(position.getLocalCharIndex());

		//
		return result;
	}

	/**
	 * Deep copy and shallow copy have no sense about a list of immutable objects.
	 * 
	 * @return the string list
	 */
	@Override
	public StringList clone()
	{
		StringList result;

		result = new StringList(size());

		for (String string : this)
		{
			result.add(string);
		}

		//
		return result;
	}

	/**
	 * Contains.
	 * 
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean contains(final CharSequence target)
	{
		boolean result;

		if (super.contains(target))
		{
			result = true;
		}
		else
		{
			result = super.contains(target.toString());
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.ArrayList#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(final Object target)
	{
		boolean result;

		if (super.contains(target))
		{
			result = true;
		}
		else
		{
			result = super.contains(target.toString());
		}

		//
		return result;
	}

	/**
	 * Contains any.
	 * 
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean containsAny(final Collection<String> target)
	{
		boolean result;

		if (target == null)
		{
			result = false;
		}
		else
		{
			boolean ended = false;
			Iterator<String> iterator = target.iterator();
			result = false;
			while (!ended)
			{
				if (iterator.hasNext())
				{
					String current = iterator.next();

					if (this.contains(current))
					{
						ended = true;
						result = true;
					}
				}
				else
				{
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Contains any.
	 * 
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean containsAny(final String... target)
	{
		boolean result;

		if (target == null)
		{
			result = false;
		}
		else
		{
			boolean ended = false;
			int index = 0;
			result = false;
			while (!ended)
			{
				if (index < target.length)
				{
					String current = target[index];

					if (this.contains(current))
					{
						ended = true;
						result = true;
					}
					else
					{
						index += 1;
					}
				}
				else
				{
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Contains any ignore case.
	 * 
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean containsAnyIgnoreCase(final Collection<String> target)
	{
		boolean result;

		if (target == null)
		{
			result = false;
		}
		else
		{
			boolean ended = false;
			Iterator<String> iterator = target.iterator();
			result = false;
			while (!ended)
			{
				if (iterator.hasNext())
				{
					String current = iterator.next();

					if (this.containsIgnoreCase(current))
					{
						ended = true;
						result = true;
					}
				}
				else
				{
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Contains any ignore case.
	 * 
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean containsAnyIgnoreCase(final String... target)
	{
		boolean result;

		if (target == null)
		{
			result = false;
		}
		else
		{
			boolean ended = false;
			int index = 0;
			result = false;
			while (!ended)
			{
				if (index < target.length)
				{
					String current = target[index];

					if (this.containsIgnoreCase(current))
					{
						ended = true;
						result = true;
					}
					else
					{
						index += 1;
					}
				}
				else
				{
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Contains blank.
	 * 
	 * @return true, if successful
	 */
	public boolean containsBlank()
	{
		boolean result;

		boolean ended = false;
		Iterator<String> iterator = iterator();
		result = false;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				String line = iterator.next();

				if (StringUtils.isBlank(line))
				{
					ended = true;
					result = true;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Contains empty.
	 * 
	 * @return true, if successful
	 */
	public boolean containsEmpty()
	{
		boolean result;

		boolean ended = false;
		Iterator<String> iterator = iterator();
		result = false;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				String line = iterator.next();

				if (StringUtils.isEmpty(line))
				{
					ended = true;
					result = true;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Contains ignore case.
	 * 
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean containsIgnoreCase(final CharSequence target)
	{
		boolean result;

		boolean ended = false;
		Iterator<String> iterator = iterator();
		result = false;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				String line = iterator.next();

				if (StringUtils.equalsIgnoreCase(target, line))
				{
					ended = true;
					result = true;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Contains null.
	 * 
	 * @return true, if successful
	 */
	public boolean containsNull()
	{
		boolean result;

		boolean ended = false;
		Iterator<String> iterator = iterator();
		result = false;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				String line = iterator.next();

				if (line == null)
				{
					ended = true;
					result = true;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by index.
	 * 
	 * @param id
	 *            the id
	 * @return the by index
	 */
	public String getByIndex(final int id)
	{
		String result;

		result = this.get(id);

		//
		return result;
	}

	/**
	 * Gets the first.
	 * 
	 * @return the first
	 */
	public String getFirst()
	{
		String result;

		if (this.size() == 0)
		{
			result = null;
		}
		else
		{
			result = get(0);
		}

		//
		return result;
	}

	/**
	 * Gets the last.
	 *
	 * @return the last
	 */
	public String getLast()
	{
		String result;

		if (this.size() == 0)
		{
			result = null;
		}
		else
		{
			result = get(this.size() - 1);
		}

		//
		return result;
	}

	/**
	 * Gets the longest bytes line.
	 * 
	 * @return the longest bytes line
	 */
	public String getLongestBytesLine()
	{
		String result;

		if (isEmpty())
		{
			result = null;
		}
		else
		{
			int max = 0;
			result = "";
			for (String line : this)
			{
				if (line.getBytes().length > max)
				{
					max = line.length();
					result = line;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the longest line.
	 * 
	 * @return the longest line
	 */
	public String getLongestLine()
	{
		String result;

		if (isEmpty())
		{
			result = null;
		}
		else
		{
			int max = 0;
			result = "";
			for (String line : this)
			{
				if (line.length() > max)
				{
					max = line.length();
					result = line;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the not blank.
	 * 
	 * @return the not blank
	 */
	public StringList getNotBlank()
	{
		StringList result;

		result = new StringList();

		for (String line : this)
		{
			if (StringUtils.isNotBlank(line))
			{
				result.add(line);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the not empty.
	 * 
	 * @return the not empty
	 */
	public StringList getNotEmpty()
	{
		StringList result;

		result = new StringList();

		for (String line : this)
		{
			if (StringUtils.isNotEmpty(line))
			{
				result.add(line);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the not null.
	 * 
	 * @return the not null
	 */
	public StringList getNotNull()
	{
		StringList result;

		result = new StringList();

		for (String line : this)
		{
			if (line != null)
			{
				result.add(line);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the shortest bytes line.
	 * 
	 * @return the shortest bytes line
	 */
	public String getShortestBytesLine()
	{
		String result;

		if (isEmpty())
		{
			result = null;
		}
		else
		{
			int min = Integer.MAX_VALUE;
			result = null;
			for (String line : this)
			{
				if (line.getBytes().length < min)
				{
					min = line.length();
					result = line;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the shortest line.
	 * 
	 * @return the shortest line
	 */
	public String getShortestLine()
	{
		String result;

		if (isEmpty())
		{
			result = null;
		}
		else
		{
			int min = Integer.MAX_VALUE;
			result = null;
			for (String line : this)
			{
				if (line.length() < min)
				{
					min = line.length();
					result = line;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Index of.
	 * 
	 * @param index
	 *            the index
	 * @return the string list char position
	 */
	public StringListCharPosition indexOf(final int index)
	{
		StringListCharPosition result;

		if ((index < 0) || (index >= length()))
		{
			throw new StringIndexOutOfBoundsException(index);
		}
		else
		{
			boolean ended = false;
			int stringIndex = 0;
			int currentLength = 0;
			result = null;
			while (!ended)
			{
				if (index < currentLength + get(stringIndex).length())
				{
					ended = true;
					result = new StringListCharPosition(index, stringIndex, index - currentLength);
				}
				else
				{
					stringIndex += 1;
					currentLength += get(stringIndex).length();
				}
			}

		}

		//
		return result;
	}

	/**
	 * Index of next line not null.
	 * 
	 * @param startIndex
	 *            the start index
	 * @return the index of the next line not null, -1 otherwise.
	 */
	public int indexOfNextLineNotNull(final int startIndex)
	{
		int result;

		boolean ended = false;
		int currentIndex = startIndex;
		result = -1;
		while (!ended)
		{
			if (currentIndex >= this.size())
			{
				ended = true;
				result = -1;
			}
			else
			{
				if (this.get(currentIndex) == null)
				{
					ended = true;
					result = currentIndex;
				}
				else
				{
					currentIndex += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is out of bounds.
	 * 
	 * @param position
	 *            the position
	 * @return true, if is out of bounds
	 */
	public boolean isOutOfBounds(final StringListCharPosition position)
	{
		boolean result;

		if (position == null)
		{
			result = true;
		}
		else
		{
			if (isOutOfList(position))
			{
				result = true;
			}
			else if (isOutOfLine(position))
			{
				result = true;
			}
			else
			{
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is out of line.
	 * 
	 * @param position
	 *            the position
	 * @return true, if is out of line
	 */
	public boolean isOutOfLine(final StringListCharPosition position)
	{
		boolean result;

		if (position == null)
		{
			result = true;
		}
		else
		{
			if (position.getLocalCharIndex() >= this.get(position.getStringIndex()).length())
			{
				result = true;
			}
			else
			{
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is out of list.
	 * 
	 * @param position
	 *            the position
	 * @return true, if is out of list
	 */
	public boolean isOutOfList(final StringListCharPosition position)
	{
		boolean result;

		if (position == null)
		{
			result = true;
		}
		else
		{
			if (position.getStringIndex() >= this.size())
			{
				result = true;
			}
			else
			{
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Iterator of char.
	 * 
	 * @return the iterator
	 */
	public Iterator<Character> iteratorOfChar()
	{
		Iterator<Character> result;

		result = new StringListCharIterator(this);

		//
		return result;
	}

	/**
	 * Returns the length of the string list concatenation. Null strings are
	 * ignored.
	 * 
	 * @return the int
	 */
	@Override
	public int length()
	{
		int result = 0;

		for (String string : this)
		{
			if (string != null)
			{
				result += string.length();
			}
		}

		//
		return result;
	}

	/**
	 * Merge all strings of the list into in single string. At the end of operation,
	 * the new string is the first of the list and the size list is 1.
	 * 
	 * @return the string list
	 */
	public StringList merge()
	{
		StringList result;

		String merge = this.toString();

		clear();
		add(merge);

		result = this;

		//
		return result;
	}

	/**
	 * Removes the last element of the list.
	 * 
	 * @return This list.
	 */
	public StringList removeLast()
	{
		StringList result;

		if (this.size() > 0)
		{
			this.remove(this.size() - 1);
		}

		result = this;

		//
		return result;
	}

	/**
	 * Extends the list copying the last element a number of time.
	 * 
	 * @param count
	 *            The number of time to copy the last element.
	 * 
	 * @return This list.
	 */
	public StringList repeatLast(final int count)
	{
		StringList result;

		//
		if ((this.size() != 0) && (count > 0))
		{
			//
			this.ensureCapacity(this.size() + count);

			//
			String last = getLast();

			//
			for (int index = 0; index < count; index++)
			{
				this.append(last);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * Sorts this list.
	 * 
	 * @return This List.
	 */
	public StringList reverse()
	{
		StringList result;

		Collections.reverse(this);

		//
		result = this;

		//
		return result;
	}

	/**
	 * Shrink all string of the list to only one string.
	 * 
	 * @return the string list
	 */
	public StringList shrink()
	{
		StringList result;

		if (!isEmpty())
		{
			String shrinked = toString();
			clear();
			add(shrinked);
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * Sorts this list.
	 * 
	 * @return This List.
	 */
	public StringList sort()
	{
		StringList result;

		sort(Collator.getInstance());

		//
		result = this;

		//
		return result;
	}

	/**
	 * Sorts this list.
	 *
	 * @param comparator
	 *            the comparator
	 * @return this string list
	 */
	public StringList sortBy(final Comparator<? super String> comparator)
	{
		StringList result;

		sort(comparator);

		//
		result = this;

		//
		return result;
	}

	/**
	 * Sorts this list.
	 * 
	 * @return This List.
	 */
	public StringList sortByLength()
	{
		StringList result;

		Collections.sort(this, StringLengthComparator.instance());

		//
		result = this;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CharSequence subSequence(final int start, final int end)
	{
		StringList result;

		result = substring(start, end);

		//
		return result;
	}

	/**
	 * Extracts a substring from this list.
	 * 
	 * @param beginIndex
	 *            The first character of the substring.
	 * @param endIndex
	 *            The last character of the substring.
	 * 
	 * @return The sublist targeted.
	 */
	public StringList substring(final int beginIndex, final int endIndex)
	{
		StringList result;

		if (beginIndex < 0)
		{
			throw new StringIndexOutOfBoundsException(beginIndex);
		}
		else if (endIndex > length())
		{
			throw new StringIndexOutOfBoundsException(endIndex);
		}
		else if (beginIndex > endIndex)
		{
			throw new StringIndexOutOfBoundsException(endIndex - beginIndex);
		}
		else if (beginIndex == endIndex)
		{
			result = new StringList();
		}
		else
		{
			//
			result = new StringList();

			//
			StringListCharPosition startPosition = indexOf(beginIndex);
			StringListCharPosition endPosition = indexOf(endIndex);

			//
			if (startPosition.getStringIndex() == endPosition.getStringIndex())
			{
				String source = get(startPosition.getStringIndex());
				String target = source.substring(startPosition.getLocalCharIndex(), endPosition.getLocalCharIndex());
				result.append(target);
			}
			else
			{
				//
				{
					String source = get(startPosition.getStringIndex());
					String target = source.substring(startPosition.getLocalCharIndex());
					result.append(target);
				}

				//
				for (int stringIndex = startPosition.getStringIndex() + 1; stringIndex < endPosition.getStringIndex(); stringIndex++)
				{
					String target = get(stringIndex);
					result.append(target);
				}

				//
				{
					String source = get(endPosition.getStringIndex());
					String target = source.substring(0, endPosition.getLocalCharIndex());
					result.append(target);
				}
			}
		}

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		String result;

		if (size() == 1)
		{
			//
			result = get(0);

		}
		else
		{
			//
			StringBuffer buffer = new StringBuffer(length());

			for (String string : this)
			{
				buffer.append(string);
			}

			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * To string.
	 * 
	 * @param prefix
	 *            the prefix
	 * @param separator
	 *            the separator
	 * @param postfix
	 *            the postfix
	 * @return the string
	 */
	public String toString(final String prefix, final String separator, final String postfix)
	{
		String result;

		StringList buffer = new StringList(1 + size() * 2 + 1);

		buffer.append(prefix);

		for (String string : this)
		{
			buffer.append(string);
			buffer.append(separator);
		}

		if (separator != null)
		{
			buffer.removeLast();
		}

		buffer.append(postfix);

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * Returns an array containing all of the strings in this list in proper
	 * sequence (from first to last element).
	 * 
	 * @return the string[]
	 */
	public String[] toStringArray()
	{
		String[] result;

		result = new String[this.size()];

		for (int index = 0; index < size(); index++)
		{
			result[index] = get(index);
		}

		//
		return result;
	}

	/**
	 * Returns a string containing the concatenation of the strings of this list.
	 * Between each string of this list, a separator argument string is concatenated
	 * too.
	 * 
	 * @param separator
	 *            the separator
	 * @return the string
	 */
	public String toStringSeparatedBy(final String separator)
	{
		String result;

		StringList buffer = new StringList(this.size() * 2);

		for (String string : this)
		{
			buffer.append(string);
			buffer.append(separator);
		}

		if (separator != null)
		{
			buffer.removeLast();
		}

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * To string with bracket.
	 * 
	 * @return the string
	 */
	public String toStringWithBracket()
	{
		String result;

		result = toString("[", ",", "]");

		//
		return result;
	}

	/**
	 * To string with brackets.
	 * 
	 * @return the string
	 */
	public String toStringWithBrackets()
	{
		String result;

		StringList buffer = new StringList();

		for (String string : this)
		{
			buffer.append("[").append(string).append("]");
		}

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * To string with commas.
	 * 
	 * @return the string
	 */
	public String toStringWithCommas()
	{
		String result;

		result = this.toStringSeparatedBy(",");

		//
		return result;
	}

	/**
	 * To string with french commas.
	 * 
	 * @return the string
	 */
	public String toStringWithFrenchCommas()
	{
		String result;

		result = this.toStringSeparatedBy(", ");

		//
		return result;
	}

}
