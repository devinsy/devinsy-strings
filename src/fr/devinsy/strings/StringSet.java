/*
 * Copyright (C) 2014-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.util.Collection;
import java.util.HashSet;

/**
 * This class is a set of String objects with specific methods. It makes
 * possible to build a string without any copy. The goal is to make easier the
 * use of set of strings.
 */
public class StringSet extends HashSet<String>
{
	private static final long serialVersionUID = 6674838743930005326L;

	/**
	 * Instantiates a new string set.
	 */
	public StringSet()
	{
		super();
	}

	/**
	 * Constructs a list of string of the specified collection, in the order they
	 * are returned by the collection's iterator.
	 * 
	 * @param source
	 *            the source
	 */
	public StringSet(final Collection<String> source)
	{
		super(source.size());

		if (source != null)
		{
			//
			for (String string : source)
			{
				this.add(string);
			}
		}
	}

	/**
	 * Instantiates a new string set.
	 * 
	 * @param initialCapacity
	 *            the initial capacity
	 */
	public StringSet(final int initialCapacity)
	{
		super(initialCapacity);
	}

	/**
	 * Instantiates a new string set.
	 * 
	 * @param source
	 *            the source
	 */
	public StringSet(final String[] source)
	{
		super();

		if (source != null)
		{
			//
			for (String string : source)
			{
				this.add(string);
			}
		}
	}

	/**
	 * Instantiates a new string set.
	 * 
	 * @param source
	 *            the source
	 */
	public StringSet(final StringSet source)
	{
		super();

		if (source != null)
		{
			//
			for (String string : source)
			{
				this.add(string);
			}
		}
	}

	/**
	 * Check null parameter before add.
	 * 
	 * @param string
	 *            the string
	 * @return true, if successful
	 */
	@Override
	public boolean add(final String string)
	{
		boolean result;

		if (string != null)
		{
			//
			result = super.add(string);
		}
		else
		{
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * Adds the.
	 * 
	 * @param strings
	 *            the strings
	 * @return the string set
	 */
	public StringSet add(final StringSet strings)
	{
		StringSet result;

		if (strings != null)
		{
			for (String string : strings)
			{
				this.add(string);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * Deep copy and shallow copy have no sense about a list of immutable objects.
	 * 
	 * @return the string set
	 */
	@Override
	public StringSet clone()
	{
		StringSet result;

		result = new StringSet(size());

		for (String string : this)
		{
			result.add(string);
		}

		//
		return result;
	}

	/**
	 * Length.
	 * 
	 * @return the int
	 */
	public int length()
	{
		int result = 0;

		for (String string : this)
		{
			result += string.length();
		}

		//
		return result;
	}

	/**
	 * Merge.
	 * 
	 * @return the string set
	 */
	public StringSet merge()
	{
		StringSet result;

		StringBuffer buffer = new StringBuffer(length());

		for (String string : this)
		{
			buffer.append(string);
		}

		String merge = buffer.toString();

		clear();
		add(merge);

		//
		result = this;
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param character
	 *            the character
	 * @return the string set
	 */
	public StringSet put(final char character)
	{
		StringSet result;

		this.add(String.valueOf(character));

		result = this;

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param value
	 *            the value
	 * @return the string set
	 */
	public StringSet put(final double value)
	{
		StringSet result;

		result = this.put(String.valueOf(value));

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param value
	 *            the value
	 * @return the string set
	 */
	public StringSet put(final int value)
	{
		StringSet result;

		result = this.put(String.valueOf(value));

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param value
	 *            the value
	 * @return the string set
	 */
	public StringSet put(final long value)
	{
		StringSet result;

		result = this.put(String.valueOf(value));

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param value
	 *            the value
	 * @return the string set
	 */
	public StringSet put(final Object value)
	{
		StringSet result;

		if (value != null)
		{
			this.put(value.toString());
		}

		result = this;

		//
		return result;
	}

	/**
	 * Check null parameter before add.
	 * 
	 * @param string
	 *            the string
	 * @return the string set
	 */
	public StringSet put(final String string)
	{
		StringSet result;

		if (string != null)
		{
			this.add(string);
		}

		result = this;

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param strings
	 *            the strings
	 * @return the string set
	 */
	public StringSet put(final String... strings)
	{
		StringSet result;

		if (strings != null)
		{
			for (String string : strings)
			{
				this.put(string);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param strings
	 *            the strings
	 * @return the string set
	 */
	public StringSet put(final StringList strings)
	{
		StringSet result;

		if (strings != null)
		{
			for (String string : strings)
			{
				this.put(string);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * Put.
	 * 
	 * @param strings
	 *            the strings
	 * @return the string set
	 */
	public StringSet put(final StringSet strings)
	{
		StringSet result;

		if (strings != null)
		{
			for (String string : strings)
			{
				this.put(string);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * Put nullable.
	 * 
	 * @param value
	 *            the value
	 * @return the string set
	 */
	public StringSet putNullable(final Object value)
	{
		StringSet result;

		if (value == null)
		{
			super.add((String) null);
		}
		else
		{
			this.put(value.toString());
		}

		result = this;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = toStringList().toStringWithCommas();

		//
		return result;
	}

	/**
	 * To string list.
	 * 
	 * @return the string list
	 */
	public StringList toStringList()
	{
		StringList result;

		result = new StringList(this);

		//
		return result;
	}
}
