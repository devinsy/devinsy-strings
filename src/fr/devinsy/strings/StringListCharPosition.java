/*
 * Copyright (C) 2013-2015,2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

/**
 * This class manages a char position in a StringList object.
 * 
 * @author Christian P. Momon
 */
public class StringListCharPosition
{
	private int charIndex;
	private int stringIndex;
	private int localCharIndex;

	/**
	 * Instantiates a new string list char position.
	 */
	public StringListCharPosition()
	{
		this.charIndex = 0;
		this.stringIndex = 0;
		this.localCharIndex = 0;
	}

	/**
	 * Instantiates a new string list char position.
	 * 
	 * @param index
	 *            the index
	 * @param stringIndex
	 *            the string index
	 * @param localIndex
	 *            the local index
	 */
	public StringListCharPosition(final int index, final int stringIndex, final int localIndex)
	{
		this.charIndex = index;
		this.stringIndex = stringIndex;
		this.localCharIndex = localIndex;
	}

	/**
	 * Instantiates a new string list char position.
	 * 
	 * @param source
	 *            the source
	 */
	public StringListCharPosition(final StringListCharPosition source)
	{
		this.charIndex = source.getCharIndex();
		this.stringIndex = source.getStringIndex();
		this.localCharIndex = source.getLocalCharIndex();
	}

	/**
	 * Gets the char index.
	 * 
	 * @return the char index
	 */
	public int getCharIndex()
	{
		return this.charIndex;
	}

	/**
	 * Gets the local char index.
	 * 
	 * @return the local char index
	 */
	public int getLocalCharIndex()
	{
		return this.localCharIndex;
	}

	/**
	 * Gets the string index.
	 * 
	 * @return the string index
	 */
	public int getStringIndex()
	{
		return this.stringIndex;
	}

	/**
	 * Next.
	 */
	public void next()
	{
		this.charIndex += 1;
		this.localCharIndex += 1;
	}

	/**
	 * Next end of line.
	 */
	public void nextEndOfLine()
	{
		this.localCharIndex = 0;
		this.stringIndex += 1;
	}

	/**
	 * Sets the char index.
	 * 
	 * @param charIndex
	 *            the new char index
	 */
	public void setCharIndex(final int charIndex)
	{
		this.charIndex = charIndex;
	}

	/**
	 * Sets the local char index.
	 * 
	 * @param localCharIndex
	 *            the new local char index
	 */
	public void setLocalCharIndex(final int localCharIndex)
	{
		this.localCharIndex = localCharIndex;
	}

	/**
	 * Sets the string index.
	 * 
	 * @param stringIndex
	 *            the new string index
	 */
	public void setStringIndex(final int stringIndex)
	{
		this.stringIndex = stringIndex;
	}
}
