/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.util.Comparator;

/**
 * The Class StringLengthComparator.
 */
public class StringLengthComparator implements Comparator<String>
{
	private static StringLengthComparator instance;

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final String alpha, final String bravo)
	{
		int result;

		//
		Integer alphaValue;
		if (alpha == null)
		{
			alphaValue = null;
		}
		else
		{
			alphaValue = alpha.length();
		}

		//
		Integer bravoValue;
		if (bravo == null)
		{
			bravoValue = null;
		}
		else
		{
			bravoValue = bravo.length();
		}

		//
		result = compare(alphaValue, bravoValue);

		//
		return result;
	}

	/**
	 * This method compares two nullable string values.
	 * 
	 * The comparison manages the local language alphabet order.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  &lt; 0
	 * compare(alpha, null)  &gt; 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Integer alpha, final Integer bravo)
	{
		int result;

		//
		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = -1;
		}
		else if (bravo == null)
		{
			result = +1;
		}
		else
		{
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * Instance.
	 * 
	 * @return the string length comparator
	 */
	public static StringLengthComparator instance()
	{
		StringLengthComparator result;

		if (instance == null)
		{
			instance = new StringLengthComparator();
		}

		result = instance;

		//
		return result;
	}

}
