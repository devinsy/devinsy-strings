/*
 * Copyright (C) 2013-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * The Class StringListReader.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class StringListReader extends Reader
{
	private StringList in;
	private StringListCharIterator iterator;

	/**
	 * Instantiates a new string list reader.
	 * 
	 * @param in
	 *            the in
	 */
	public StringListReader(final StringList in)
	{
		this.in = in;
		this.iterator = new StringListCharIterator(in);
	}

	/* (non-Javadoc)
	 * @see java.io.Reader#close()
	 */
	@Override
	public void close()
	{
	}

	/* (non-Javadoc)
	 * @see java.io.Reader#read(char[], int, int)
	 */
	@Override
	public synchronized int read(final char[] cbuf, final int off, final int len) throws IOException
	{
		int result;

		BufferedReader a;

		if ((off < 0) || (off > cbuf.length) || (len < 0) || ((off + len) > cbuf.length) || ((off + len) < 0))
		{
			throw new IndexOutOfBoundsException();
		}
		else if (len == 0)
		{
			result = 0;
		}
		else if (this.iterator.hasNext())
		{
			//
			result = 0;

			// Read off characters.
			{
				boolean ended = false;
				int offCount = 0;
				while (!ended)
				{
					if ((offCount < off) && (this.iterator.hasNext()))
					{
						this.iterator.next();
						offCount += 1;
						result += 1;
					}
					else
					{
						ended = true;
					}
				}
			}

			// Read len characters.
			{
				boolean ended = false;
				int lenCount = 0;
				while (!ended)
				{
					if ((lenCount < len) && (this.iterator.hasNext()))
					{
						char letter = this.iterator.next();
						cbuf[lenCount] = letter;
						lenCount += 1;
						result += 1;
					}
					else
					{
						ended = true;
					}
				}
			}
		}
		else
		{
			result = -1;
		}

		//
		return result;
	}
}
