/*
 * Copyright (C) 2008-2010,2013-2016,2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

/**
 * The {@code StringUtils} class defines helper methods to string collection.
 * 
 * Operations that are null safe.
 */
public class StringsUtils
{
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Instantiates a new stringst utils.
	 */
	private StringsUtils()
	{
	}

	/**
	 * Capitalize.
	 *
	 * @param source
	 *            the source
	 */
	public static void capitalize(final StringList source)
	{
		if (source != null)
		{
			for (int index = 0; index < source.size(); index++)
			{
				source.set(index, StringUtils.capitalize(source.get(index)));
			}
		}
	}

	/**
	 * Capitalize.
	 * 
	 * @param source
	 *            the source
	 */
	public static void capitalize(final StringSet source)
	{
		if (source != null)
		{
			StringList values = new StringList(source);
			capitalize(values);
			source.clear();
			source.addAll(values);
		}
	}

	/**
	 * Builds a string concatenating many times a source string.
	 * 
	 * @param source
	 *            The string to concatenate several time.
	 * @param multiplier
	 *            The number of concatenate to produce.
	 * @return the string
	 */
	public static String concatenate(final String source, final int multiplier)
	{
		String result;

		result = repeat(source, multiplier).toString();

		//
		return result;
	}

	/**
	 * Contains any.
	 * 
	 * @param token
	 *            the token
	 * @param strings
	 *            the strings
	 * @return true, if successful
	 */
	public static boolean containsAny(final String token, final String... strings)
	{
		boolean result;

		boolean ended = false;
		int index = 0;
		result = false;
		while (!ended)
		{
			if (index < strings.length)
			{
				String current = strings[index];

				if (StringUtils.contains(token, current))
				{
					ended = true;
					result = true;
				}
				else
				{
					index += 1;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Contains any ignore case.
	 * 
	 * @param token
	 *            the token
	 * @param strings
	 *            the strings
	 * @return true, if successful
	 */
	public static boolean containsAnyIgnoreCase(final String token, final String... strings)
	{
		boolean result;

		boolean ended = false;
		int index = 0;
		result = false;
		while (!ended)
		{
			if (index < strings.length)
			{
				String current = strings[index];

				if (StringUtils.containsIgnoreCase(token, current))
				{
					ended = true;
					result = true;
				}
				else
				{
					index += 1;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Contains blank.
	 * 
	 * @param source
	 *            the source
	 * @return true, if successful
	 */
	public static boolean containsBlank(final Collection<String> source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else if (source instanceof StringList)
		{
			result = ((StringList) source).containsBlank();
		}
		else
		{
			result = new StringList(source).containsBlank();
		}

		//
		return result;
	}

	/**
	 * Contains blank.
	 * 
	 * @param source
	 *            the source
	 * @return true, if successful
	 */
	public static boolean containsBlank(final String... source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else
		{
			result = new StringList(source).containsBlank();
		}

		//
		return result;
	}

	/**
	 * Contains empty.
	 * 
	 * @param source
	 *            the source
	 * @return true, if successful
	 */
	public static boolean containsEmpty(final Collection<String> source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else if (source instanceof StringList)
		{
			result = ((StringList) source).containsEmpty();
		}
		else
		{
			result = new StringList(source).containsEmpty();
		}

		//
		return result;
	}

	/**
	 * Contains empty.
	 * 
	 * @param source
	 *            the source
	 * @return true, if successful
	 */
	public static boolean containsEmpty(final String... source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else
		{
			result = new StringList(source).containsEmpty();
		}

		//
		return result;
	}

	/**
	 * Contains null.
	 * 
	 * @param source
	 *            the source
	 * @return true, if successful
	 */
	public static boolean containsNull(final Collection<String> source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else if (source instanceof StringList)
		{
			result = ((StringList) source).containsNull();
		}
		else
		{
			result = new StringList(source).containsNull();
		}

		//
		return result;
	}

	/**
	 * Contains null.
	 * 
	 * @param source
	 *            the source
	 * @return true, if successful
	 */
	public static boolean containsNull(final String... source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else
		{
			result = new StringList(source).containsNull();
		}

		//
		return result;
	}

	/**
	 * Checks if is contained.
	 * 
	 * @param token
	 *            the token
	 * @param strings
	 *            the strings
	 * @return true, if is contained
	 */
	public static boolean equalsAny(final String token, final String... strings)
	{
		boolean result;

		result = new StringList(strings).contains(token);

		//
		return result;
	}

	/**
	 * Checks if is contained ignore case.
	 * 
	 * @param token
	 *            the token
	 * @param strings
	 *            the strings
	 * @return true, if is contained ignore case
	 */
	public static boolean equalsAnyIgnoreCase(final String token, final String... strings)
	{
		boolean result;

		result = new StringList(strings).containsIgnoreCase(token);

		//
		return result;
	}

	/**
	 * Find longest.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String findLongest(final Collection<String> source)
	{
		String result;

		if ((source == null) || (source.isEmpty()))
		{
			result = null;
		}
		else
		{
			int value = 0;
			result = null;

			for (String string : source)
			{
				if ((string != null) && (string.length() > value))
				{
					value = string.length();
					result = string;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Find longest length.
	 * 
	 * @param source
	 *            the source
	 * @return the int
	 */
	public static int findLongestLength(final Collection<String> source)
	{
		int result;

		String string = findLongest(source);
		if (string == null)
		{
			result = 0;
		}
		else
		{
			result = string.length();
		}

		//
		return result;
	}

	/**
	 * Find smallest.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String findShortest(final Collection<String> source)
	{
		String result;

		if ((source == null) || (source.isEmpty()))
		{
			result = null;
		}
		else
		{
			int value = Integer.MAX_VALUE;
			result = null;

			for (String string : source)
			{
				if ((string != null) && (string.length() < value))
				{
					value = string.length();
					result = string;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Find smallest length.
	 * 
	 * @param source
	 *            the source
	 * @return the int
	 */
	public static int findShortestLength(final Collection<String> source)
	{
		int result;

		String string = findShortest(source);
		if (string == null)
		{
			result = 0;
		}
		else
		{
			result = string.length();
		}

		//
		return result;
	}

	/**
	 * Gets the complement.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 * @return the complement
	 */
	public static StringSet getComplement(final Collection<String> alpha, final Collection<String> bravo)
	{
		StringSet result;

		result = getDifference(bravo, alpha);

		//
		return result;
	}

	/**
	 * Gets the difference.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 * @return the difference
	 */
	public static StringSet getDifference(final Collection<String> alpha, final Collection<String> bravo)
	{
		StringSet result;

		result = new StringSet(alpha);
		result.removeAll(bravo);

		//
		return result;
	}

	/**
	 * Gets the disjunction.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 * @return the disjunction
	 */
	public static StringSet getDisjunction(final Collection<String> alpha, final Collection<String> bravo)
	{
		StringSet result;

		result = new StringSet();

		for (String string : alpha)
		{
			if (!bravo.contains(string))
			{
				result.add(string);
			}
		}

		for (String string : bravo)
		{
			if (!alpha.contains(string))
			{
				result.add(string);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the intersection of.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 * @return the intersection of
	 */
	public static StringSet getIntersectionOf(final Collection<String> alpha, final Collection<String> bravo)
	{
		StringSet result;

		result = new StringSet();

		for (String string : alpha)
		{
			if (bravo.contains(string))
			{
				result.add(string);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the union of.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 * @return the union of
	 */
	public static StringSet getUnionOf(final Collection<String> alpha, final Collection<String> bravo)
	{
		StringSet result;

		result = new StringSet(alpha);

		for (String string : bravo)
		{
			result.add(string);
		}

		//
		return result;
	}

	/**
	 * Checks if is blank.
	 *
	 * @param source
	 *            the source
	 * @return true, if is blank
	 */
	public static boolean isBlank(final Collection<String> source)
	{
		boolean result;

		if (source == null)
		{
			result = true;
		}
		else
		{
			Iterator<String> iterator = source.iterator();
			boolean ended = false;
			result = false;
			while (!ended)
			{
				if (iterator.hasNext())
				{
					String value = iterator.next();

					if (StringUtils.isNotBlank(value))
					{
						ended = true;
						result = false;
					}
				}
				else
				{
					ended = true;
					result = true;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is empty.
	 * 
	 * @param source
	 *            the source
	 * @return true, if is empty
	 */
	public static boolean isEmptyFully(final Collection<String> source)
	{
		boolean result;

		if ((source == null) || (source.isEmpty()))
		{
			result = true;
		}
		else
		{
			Iterator<String> iterator = source.iterator();
			boolean ended = false;
			result = false;
			while (!ended)
			{
				if (iterator.hasNext())
				{
					String value = iterator.next();
					if (StringUtils.isNotEmpty(value))
					{
						ended = true;
						result = false;
					}
				}
				else
				{
					ended = true;
					result = true;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList load(final File source) throws IOException
	{
		StringList result;

		result = load(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param file
	 *            the file
	 * @param charsetName
	 *            the charset name
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList load(final File file, final String charsetName) throws IOException
	{
		StringList result;

		//
		result = new StringList();

		//
		read(result, new FileInputStream(file), charsetName);

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList load(final URL source) throws IOException
	{
		StringList result;

		result = load(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param source
	 *            the source
	 * @param charsetName
	 *            the charset name
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList load(final URL source, final String charsetName) throws IOException
	{
		StringList result;

		//
		result = new StringList();

		//
		read(result, source.openStream(), charsetName);

		//
		return result;
	}

	/**
	 * Lower case.
	 * 
	 * @param source
	 *            the source
	 */
	public static void lowerCase(final StringList source)
	{
		if (source != null)
		{
			for (int index = 0; index < source.size(); index++)
			{
				source.set(index, StringUtils.lowerCase(source.get(index)));
			}
		}
	}

	/**
	 * Lower case.
	 * 
	 * @param source
	 *            the source
	 */
	public static void lowerCase(final StringSet source)
	{
		if (source != null)
		{
			StringList values = new StringList(source);
			lowerCase(values);
			source.clear();
			source.addAll(values);
		}
	}

	/**
	 * Read.
	 * 
	 * @param out
	 *            the out
	 * @param is
	 *            the is
	 * @param charsetName
	 *            the charset name
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void read(final StringList out, final InputStream is, final String charsetName) throws IOException
	{
		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(is, charsetName));

			boolean ended = false;

			while (!ended)
			{
				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					out.append(line);
				}
			}
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException exception)
			{
				exception.printStackTrace();
			}
		}
	}

	/**
	 * Builds a string list concatenating many times a source string.
	 * 
	 * @param source
	 *            The string to concatenate several time.
	 * @param multiplier
	 *            The number of concatenate to produce.
	 * @return the string list
	 */
	public static StringList repeat(final String source, final int multiplier)
	{
		StringList result;

		result = new StringList(multiplier);
		for (int count = 0; count < multiplier; count++)
		{
			result.append(source);
		}

		//
		return result;
	}

	/**
	 * Save.
	 * 
	 * @param file
	 *            the file
	 * @param source
	 *            the source
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public static void save(final File file, final StringList source) throws UnsupportedEncodingException, FileNotFoundException
	{
		PrintWriter out = null;
		try
		{
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), DEFAULT_CHARSET_NAME));

			if (source != null)
			{
				for (String string : source)
				{
					out.println(string);
				}
			}
		}
		finally
		{
			if (out != null)
			{
				out.close();
			}
		}
	}

	/**
	 * Sorts the string list.
	 * 
	 * @param source
	 *            The string list to sort.
	 */
	public static void sort(final StringList source)
	{
		if (source != null)
		{
			source.sort();
		}
	}

	/**
	 * Swap case.
	 * 
	 * @param source
	 *            the source
	 */
	public static void swapCase(final StringList source)
	{
		if (source != null)
		{
			for (int index = 0; index < source.size(); index++)
			{
				source.set(index, StringUtils.swapCase(source.get(index)));
			}
		}
	}

	/**
	 * Swap case.
	 * 
	 * @param source
	 *            the source
	 */
	public static void swapCase(final StringSet source)
	{
		if (source != null)
		{
			StringList values = new StringList(source);
			swapCase(values);
			source.clear();
			source.addAll(values);
		}
	}

	/**
	 * Concatenates the string from an array to a string.
	 * 
	 * @param source
	 *            The string array to convert.
	 * 
	 * @return A string concatenation of the argument.
	 */
	public static String toString(final String[] source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toString();
		}

		//
		return result;
	}

	/**
	 * To string.
	 * 
	 * @param source
	 *            the source
	 * @param prefix
	 *            the prefix
	 * @param separator
	 *            the separator
	 * @param postfix
	 *            the postfix
	 * @return the string
	 */
	public static String toString(final String[] source, final String prefix, final String separator, final String postfix)
	{
		String result;

		if (source == null)
		{
			result = new StringList().toString(prefix, null, postfix);
		}
		else
		{
			result = new StringList(source).toString(prefix, separator, postfix);
		}

		//
		return result;
	}

	/**
	 * To string.
	 * 
	 * @param source
	 *            the source
	 * @param prefix
	 *            the prefix
	 * @param separator
	 *            the separator
	 * @param postfix
	 *            the postfix
	 * @return the string
	 */
	public static String toString(final StringList source, final String prefix, final String separator, final String postfix)
	{
		String result;

		if (source == null)
		{
			result = new StringList().toString(prefix, null, postfix);
		}
		else
		{
			result = source.toString(prefix, separator, postfix);
		}

		//
		return result;
	}

	/**
	 * Converts a {@code Collection<String>} to an array of {@code String}.
	 * 
	 * @param source
	 *            The string list to convert.
	 * 
	 * @return The result of the conversion.
	 */
	public static String[] toStringArray(final Collection<String> source)
	{
		String[] result;

		if (source == null)
		{
			result = new String[0];
		}
		else
		{
			result = new StringList(source).toStringArray();
		}

		//
		return result;
	}

	/**
	 * Converts a {@code StringList} to an array of {@code String}.
	 * 
	 * @param source
	 *            The string list to convert.
	 * 
	 * @return The result of the conversion.
	 */
	public static String[] toStringArray(final StringList source)
	{
		String[] result;

		if (source == null)
		{
			result = new String[0];
		}
		else
		{
			result = source.toStringArray();
		}

		//
		return result;
	}

	public static StringList toStringList(final boolean... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (boolean value : source)
			{
				result.append(value);
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final Boolean... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (Boolean value : source)
			{
				result.append(String.valueOf(value));
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final byte... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (byte value : source)
			{
				result.append(value);
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final Byte... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (Byte value : source)
			{
				result.add(String.valueOf(value));
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final char... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (char value : source)
			{
				result.append(value);
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final double... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (double value : source)
			{
				result.append(value);
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final Double... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (Double value : source)
			{
				result.add(String.valueOf(value));
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final float... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (float value : source)
			{
				result.append(value);
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final Float... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (Float value : source)
			{
				result.add(String.valueOf(value));
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final long... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (long value : source)
			{
				result.append(value);
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final Long... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (Long value : source)
			{
				result.add(String.valueOf(value));
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final short... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (short value : source)
			{
				result.append(value);
			}
		}

		//
		return result;
	}

	public static StringList toStringList(final Short... source)
	{
		StringList result;

		result = new StringList();

		if (source != null)
		{
			result.ensureCapacity(source.length);

			for (Short value : source)
			{
				result.add(String.valueOf(value));
			}
		}

		//
		return result;
	}

	/**
	 * Concatenates the string from an array to a string.
	 * 
	 * @param source
	 *            the source
	 * @return If argument is null then returns an empty string, otherwise returns a
	 *         string concatenation of the argument.
	 */
	public static String toStringNotNull(final String[] source)
	{
		String result;

		result = toString(source);

		if (result == null)
		{
			result = "";
		}

		//
		return result;
	}

	/**
	 * To string separated by.
	 * 
	 * @param source
	 *            the source
	 * @param separator
	 *            the separator
	 * @return the string
	 */
	public static String toStringSeparatedBy(final Collection<String> source, final String separator)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringSeparatedBy(separator);
		}

		//
		return result;
	}

	/**
	 * To string separated by.
	 * 
	 * @param source
	 *            the source
	 * @param separator
	 *            the separator
	 * @return the string
	 */
	public static String toStringSeparatedBy(final String[] source, final String separator)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringSeparatedBy(separator);
		}

		//
		return result;
	}

	/**
	 * To string separated by.
	 * 
	 * @param source
	 *            the source
	 * @param separator
	 *            the separator
	 * @return the string
	 */
	public static String toStringSeparatedBy(final StringList source, final String separator)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.toStringSeparatedBy(separator);
		}

		//
		return result;
	}

	/**
	 * To string with bracket.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBracket(final Collection<String> source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithBracket();
		}

		//
		return result;
	}

	/**
	 * To string with bracket.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBracket(final String[] source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithBracket();
		}

		//
		return result;
	}

	/**
	 * To string with bracket.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBracket(final StringList source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.toStringWithBracket();
		}

		//
		return result;
	}

	/**
	 * To string with bracket not null.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBracketNotNull(final Collection<String> source)
	{
		String result;

		result = toStringWithBracket(source);

		if (result == null)
		{
			result = "";
		}

		//
		return result;
	}

	/**
	 * To string with bracket not null.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBracketNotNull(final String[] source)
	{
		String result;

		result = toStringWithBracket(source);

		if (result == null)
		{
			result = "";
		}

		//
		return result;
	}

	/**
	 * To string with bracket not null.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBracketNotNull(final StringList source)
	{
		String result;

		result = toStringWithBracket(source);

		if (result == null)
		{
			result = "";
		}

		//
		return result;
	}

	/**
	 * To string with brackets.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBrackets(final Collection<String> source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithBrackets();
		}

		//
		return result;
	}

	/**
	 * To string with brackets.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBrackets(final String[] source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithBrackets();
		}

		//
		return result;
	}

	/**
	 * To string with brackets.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithBrackets(final StringList source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.toStringWithBrackets();
		}

		//
		return result;
	}

	/**
	 * To string with commas.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithCommas(final Collection<String> source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithCommas();
		}

		//
		return result;
	}

	/**
	 * To string with commas.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithCommas(final String[] source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithCommas();
		}

		//
		return result;
	}

	/**
	 * To string with commas.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithCommas(final StringList source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.toStringWithCommas();
		}

		//
		return result;
	}

	/**
	 * To string with french commas.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithFrenchCommas(final Collection<String> source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithFrenchCommas();
		}

		//
		return result;
	}

	/**
	 * To string with french commas.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithFrenchCommas(final String[] source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new StringList(source).toStringWithFrenchCommas();
		}

		//
		return result;
	}

	/**
	 * To string with french commas.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toStringWithFrenchCommas(final StringList source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = source.toStringWithFrenchCommas();
		}

		//
		return result;
	}

	/**
	 * Uncapitalize.
	 * 
	 * @param source
	 *            the source
	 */
	public static void uncapitalize(final StringList source)
	{
		if (source != null)
		{
			for (int index = 0; index < source.size(); index++)
			{
				source.set(index, StringUtils.uncapitalize(source.get(index)));
			}
		}
	}

	/**
	 * Uncapitalize.
	 * 
	 * @param source
	 *            the source
	 */
	public static void uncapitalize(final StringSet source)
	{
		if (source != null)
		{
			StringList values = new StringList(source);
			uncapitalize(values);
			source.clear();
			source.addAll(values);
		}
	}

	/**
	 * Upper case.
	 * 
	 * @param source
	 *            the source
	 */
	public static void upperCase(final StringList source)
	{
		if (source != null)
		{
			for (int index = 0; index < source.size(); index++)
			{
				source.set(index, StringUtils.upperCase(source.get(index)));
			}
		}
	}

	public static void upperCase(final StringSet source)
	{
		if (source != null)
		{
			StringList values = new StringList(source);
			upperCase(values);
			source.clear();
			source.addAll(values);
		}
	}

	/**
	 * Writes the strings of this list into a {@code PrintWriter}.
	 * 
	 * @param source
	 *            the source
	 * @param out
	 *            The {@code PrintWriter} where to write.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void writeTo(final StringList source, final java.io.PrintWriter out) throws IOException
	{
		if (source != null)
		{
			for (String string : source)
			{
				out.write(string);
			}
		}
	}

	/**
	 * Writes the strings of this list into a {@code Writer}.
	 * 
	 * @param source
	 *            the source
	 * @param out
	 *            The {@code Writer} where to write.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void writeTo(final StringList source, final java.io.Writer out) throws IOException
	{
		if (source != null)
		{
			for (String string : source)
			{
				out.write(string);
			}
		}
	}
}
