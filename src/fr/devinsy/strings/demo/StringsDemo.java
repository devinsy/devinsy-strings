/*
 * Copyright (C) 2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings.demo;

import java.util.ArrayList;
import java.util.Date;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class Demo.
 */
public class StringsDemo
{
	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		// #1
		{
			StringList strings = new StringList();

			strings.appendln("========== DEMO #1;");
			strings.appendln();
			strings.appendln("Need to concatenate arbitrary number of string?");
			strings.appendln("Don't know what will be the final size?");
			strings.appendln("So StringBuilder and StringBuffer are not efficient.");
			strings.appendln("Choose StringList!");

			System.out.println(strings.toString());
		}

		// #2
		{
			StringList strings = new StringList();

			strings.appendln("========== DEMO #2;");
			strings.appendln();
			strings.appendln("Easy to concatenate different types.");
			strings.append("Boolean:             ").append(true).append(" ").appendln(new Boolean(false));
			strings.append("Char:                ").append('e').append(" ").appendln(new Character('e'));
			strings.append("Byte :               ").append((byte) 5).append(" ").appendln(new Byte((byte) 5));
			strings.append("Short:               ").append((short) 55).append(" ").appendln(new Short((short) 55));
			strings.append("Integer:             ").append(555).append(" ").appendln(new Integer(555));
			strings.append("Long:                ").append(5555L).append(" ").appendln(new Long(5555));
			strings.append("Float:               ").append((float) 5555.5).append(" ").appendln(new Float(5555.5));
			strings.append("Double:              ").append(5555.55).append(" ").appendln(new Double(5555.55));
			strings.append("String:              ").append("abcde").append(" ").appendln(new String("abcde"));
			strings.append("Object:              ").appendln(new Date());
			strings.append("Array of booleans:   ").appendln(StringsUtils.toStringList(new boolean[] { true, false, true }));
			strings.append("Array of chars:      ").appendln(StringsUtils.toStringList(new char[] { 'a', 'b', 'c' }));
			strings.append("Array of bytes:      ").appendln(StringsUtils.toStringList(new byte[] { (byte) 1, (byte) 2, (byte) 3 }));
			strings.append("Array of shorts:     ").appendln(StringsUtils.toStringList(new short[] { (short) 1, (short) 2, (short) 3 }));
			strings.append("Array of longs:      ").appendln(StringsUtils.toStringList(new long[] { 1L, 2L, 3L }));
			strings.append("Array of strings(1): ").appendln(new String[] { "ab", "cd", "e" });
			strings.append("Array of strings(2): ").appendln("ab", "cd", "e");
			strings.append("Array of objects:    ").appendln(new Object[] { "ab", "cd", "e" });
			ArrayList<String> buffer = new ArrayList<String>();
			buffer.add("ab");
			buffer.add("cd");
			buffer.add("e");
			strings.append("Collections:         ").appendln(buffer);

			System.out.println(strings.toString());
		}

		// #3
		{
			StringList strings = new StringList();

			strings.appendln("========== DEMO #3;");
			strings.appendln();
			strings.appendln("Useful and easy methods.");
			StringList list = new StringList("red", "blue", "green");
			strings.append("Format(1):  ").appendln(list.toStringWithBracket());
			strings.append("Format(2):  ").appendln(list.toStringWithBrackets());
			strings.append("Format(3):  ").appendln(list.toStringWithCommas());
			strings.append("Format(4):  ").appendln(list.toStringWithFrenchCommas());
			strings.append("RemoveLast: ").appendln(list.removeLast().toStringWithFrenchCommas());

			System.out.println(strings.toString());
		}

		// #4
		{
			StringList strings = new StringList();

			strings.appendln("========== DEMO #4;");
			strings.appendln();
			strings.appendln("Advanced features.");
			strings.append("Boolean:           ").append(true).append(" ").appendln(new Boolean(false));

			System.out.println(strings.toString());
		}

		// #5
		{
			StringList strings = new StringList();

			strings.appendln("========== DEMO #5;");
			strings.appendln();
			strings.appendln("Funny features.");
			strings.append("Boolean:           ").append(true).append(" ").appendln(new Boolean(false));

			System.out.println(strings.toString());
		}
	}
}
