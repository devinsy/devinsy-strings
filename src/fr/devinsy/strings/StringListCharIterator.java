/*
 * Copyright (C) 2014-2015,2017-2018 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-strings.
 * 
 * Devinsy-strings is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-strings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-strings.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.strings;

import java.util.Iterator;

/**
 * The Class StringListCharIterator.
 */
public class StringListCharIterator implements Iterator<Character>
{
	private StringList source;
	private StringListCharPosition nextPosition;

	/**
	 * Instantiates a new string list char iterator.
	 * 
	 * @param source
	 *            the source
	 */
	public StringListCharIterator(final StringList source)
	{
		super();

		this.source = source;
		this.nextPosition = new StringListCharPosition();
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext()
	{
		boolean result;

		if (this.source == null)
		{
			result = false;
		}
		else
		{
			if (this.source.isOutOfBounds(this.nextPosition))
			{
				result = false;
			}
			else
			{
				result = true;
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public Character next()
	{
		Character result;

		if (this.source == null)
		{
			result = null;
		}
		else
		{
			if (hasNext())
			{
				result = this.source.charAt(this.nextPosition);

				this.nextPosition.next();

				if (this.source.isOutOfLine(this.nextPosition))
				{
					this.nextPosition.nextEndOfLine();

					while ((!this.source.isOutOfList(this.nextPosition)) && (this.source.get(this.nextPosition.getStringIndex()) == null))
					{
						this.nextPosition.nextEndOfLine();
					}
				}
			}
			else
			{
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * Next position.
	 * 
	 * @return the string list char position
	 */
	public StringListCharPosition nextPosition()
	{
		StringListCharPosition result;

		result = new StringListCharPosition(this.nextPosition);

		//
		return result;
	}

	/**
	 * Do nothing.
	 */
	@Override
	public void remove()
	{
		// TODO or not TODO?
	}
}
